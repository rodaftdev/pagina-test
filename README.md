# Aprendiendo git
## Introduccion a Git
esta es la introduccion de git
## primeros pasos **Dia 1**
  - VSC ¿ Que es un control de versiones?
  - Introducción a **Git**
  - Historia de Git
  - Instalación de Git
  - Configuración de Git
  - Comandos básicos de la terminal
    - "**git init**"
    - "**git add**" y "**git commit**"
    - "**git log**" y "**git status**"
    - "**git checkout**" y "**git reset**"
  - Ramas en Git
  - Fichero .gitignore
  - "**git diff**"
